user nginx;
worker_processes auto;
worker_rlimit_nofile 50000;
error_log /dev/stderr;
pid /run/nginx.pid;

include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 4096;
    use epoll;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for" $request_time';

    map $http_user_agent $log_ua {
        ~ELB-HealthChecker 0;
        default 1;
    }

    access_log  /dev/stdout main if=$log_ua;
    error_log   /dev/stderr;

    sendfile                  on;
    tcp_nopush                on;
    tcp_nodelay               on;
    keepalive_timeout         60;
    client_header_timeout     60;
    client_body_timeout       60;
    send_timeout              60;
    reset_timedout_connection on;
    types_hash_max_size       2048;
    client_max_body_size      512M;

    server_tokens   off;

    gzip            on;
    gzip_vary       on;
    gzip_disable    "msie6";
    gzip_proxied    any;
    gzip_min_length 1024;
    gzip_comp_level 6;
    gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

    include      /etc/nginx/mime.types;
    default_type application/octet-stream;

    include /etc/nginx/conf.d/*.conf;

    server {
        listen       80;
        server_name  _;
        root         /code/public;
        index        index.php;

        include /etc/nginx/default.d/*.conf;

        location = /favicon.ico {
            empty_gif;
            access_log off;
            log_not_found off;
        }

        location / {
            try_files $uri $uri/ /index.php$is_args$args;
            location ~ \.php$ {
                fastcgi_pass  127.0.0.1:8080;
                fastcgi_index index.php;
                include       fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
                fastcgi_param PATH_INFO $fastcgi_path_info;
            }
        }
    }
}